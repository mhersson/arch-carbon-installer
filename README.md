# Arch Carbon Installer
CLI net-installer for Arch Linux, forked from Manjaro Architect

## What is Arch Carbon Installer
Arch Carbon Installer (ACI) is an installer for Arch Linux using user defined
custom profiles. The idea is that users can build, store and share their setup
by creating and sharing their own profiles. If you are a DE/WM hopper who wants
to save your current setup for later use, want the same setup on both your
laptop and workstation, or you just created a fantastic rice you want to share
with the world. ACI will help you or anyone using the same profile to install
identical environments.


## What is it not
Even though ACI is based on Manjaro Architect it is not an attempt to bring the
full featured Manjaro Architect to Arch Linux.


## How To setup a system using ACI
* Boot the Arch Linux install media
* Get a network connection - wired should just work, for wifi configure your
connection using `iwctl` , example `iwctl --passphrase PASSPHRASE station DEVICE
connect SSID`
* Install packages needed by ACI: `pacman -Sy fzf dialog git`
* Clone ACI: `git clone --depth 1 --recurse-submodules
  https://gitlab.com/mhersson/arch-carbon-installer.git`
* Start ACI: `cd arch-carbon-installer && ./bin/setup` It's very important that
  ACI is started from within the arch-carbon-installer root directory.


## Adding profiles
ACI comes with a small set of profiles from the [arch-carbon-profiles](https://gitlab.com/mhersson/arch-carbon-profiles) repo. 
After creating a profile, copy it to `arch-carbon-installer/data/profiles` to
use it. Read more about profiles in the README of the before mentioned repo.

## The express preparation - UEFI, LVM on LUKS with keyfile
The express preparation is a preconfigured procedure to partition, format and
mount devices. It also limits the user to the "Systemd-boot (Express)" option
when choosing bootloader.

This setup requires two devices where the first, preferably a removable usb
drive, is to be used only as `/boot`, and the second to contain one LUKS
encrypted partion expanding the entire drive. The LUKS partition is encrypted
with a keyfile stored on the drive itself, and then embedded in the initramfs
image on the boot device. The encrypted LUKS partition contains a volume group
with two logical volumes; a 16 GB SWAP volume, and whatever space there is left
is used for /root. **There is no password created to open the encrypted drive.
This means if you pull the removable usb the computer will not boot, and if you
loose it, or if it breaks, there is no way to recover the data on the
computer.**


**menu overview of v0.5.1:**

```
Main Menu
|
├── Prepare Installation
|   ├── Express preparations
|   ├── Set Virtual Console
|   ├── List Devices
|   ├── Partition Disk
|   ├── Logical Volume Management
|   ├── LUKS Encryption
|   ├── RAID
|   ├── Mount Partitions
|   ├── Configure Installer Mirrorlist
|   |   ├── Edit Pacman Configuration
|   |   ├── Edit Pacman Mirror Configuration
|   |   └── Rank Mirrors by Speed
|   |
│   └── Refresh Pacman Keys
|
├── Install System
│   ├── Install Desktop Environment
│   ├── Install Bootloader
│   ├── Configure Base
|   │   ├── Generate FSTAB
|   │   ├── Set Hostname
|   │   ├── Set System Locale
|   │   ├── Set Desktop Keyboard Layout
|   │   ├── Set Timezone and Clock
|   │   ├── Set Root Password
|   │   └── Add New User(s)
|   │
│   ├── System Tweaks
|   │   ├── Enable Automatic Login
|   │   ├── Performance
|   |   │   ├── I/O schedulers
|   |   │   └── Swap configuration
|   |   │
|   │   ├── Security and systemd Tweaks
|   |   │   ├── Amend journald Logging
|   |   │   ├── Disable Coredump Logging
|   |   │   └── Restrict Access to Kernel Logs
|   |   │
|   │   └── Restrict Access to Kernel Logs
|   │
│   ├── Review Configuration Files
│   └── Chroot into Installation
|
|
└── System Rescue
    ├── Install Bootloader
    ├── Configure Base
    |   └── ... (see 'Install Desktop System')
    │
    ├── Install Custom Packages
    ├── Remove Packages
    ├── Review Configuration Files
    ├── Chroot into Installation
    ├── Data Recovery
    │   ├── Clonezilla
    │   └── Photorec
    │
    └── View System Logs
        ├── Dmesg
        ├── Pacman log
        ├── Xorg log
        └── Journalctl
```
