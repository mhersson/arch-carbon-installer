# !/bin/bash
#
# Architect Installation Framework (2016-2017)
#
# Written by Carl Duff and @mandog for Archlinux
# Heavily modified and re-written by @Chrysostomus to install Manjaro instead
# Contributors: @papajoker, @oberon and the Manjaro-Community.
#
# This program is free software, provided under the GNU General Public License
# as published by the Free Software Foundation. So feel free to copy, distribute,
# or modify it as you wish.

install_cust_pkgs() {
    echo "" > ${PACKAGES}
    pacman -Ssq | fzf -m -e --header="$_AddPkgs" --prompt="$_AddPkgsPrmpt > " --reverse >${PACKAGES} || return 0

    clear
    # If at least one package, install.
    if [[ $(cat ${PACKAGES}) != "" ]]; then
            if $hostcache; then
              pacstrap ${MOUNTPOINT} $(cat ${PACKAGES}) 2>$ERR
            else
              pacstrap -c ${MOUNTPOINT} $(cat ${PACKAGES}) 2>$ERR
            fi
            check_for_error "$FUNCNAME $(cat ${PACKAGES})" "$?"
    fi
}

rm_pgs(){
  arch-chroot /mnt "pacman -Qq" | fzf -m -e --header="$_RmPkgsMsg" --prompt="$_RmPkgsPrmpt > " --reverse > /tmp/.to_be_removed || return 0

    if [[ $(cat /tmp/.to_be_removed) != "" ]]; then
            arch_chroot "pacman -Rsn $(cat /tmp/.to_be_removed)" 2>$ERR
            check_for_error "$FUNCNAME $(cat /tmp/.to_be_removed)" "$?"
    fi
}

chroot_interactive() {

DIALOG " $_EnterChroot " --infobox "$_ChrootReturn" 0 0 
echo ""
echo ""
arch_chroot bash
}


security_menu() {
    declare -i loopmenu=1
    while ((loopmenu)); do
        local PARENT="$FUNCNAME"
        ssubmenu 4
        DIALOG " $_SecMenuTitle " --default-item ${HIGHLIGHT_SSUB} --menu "\n$_SecMenuBody\n " 0 0 4 \
          "1" "$_SecJournTitle" \
          "2" "$_SecCoreTitle" \
          "3" "$_SecKernTitle " \
          "4" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SSUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            # systemd-journald
            "1") DIALOG " $_SecJournTitle " --menu "\n$_SecJournBody\n " 0 0 7 \
                   "$_Edit" "/etc/systemd/journald.conf" \
                   "10M" "SystemMaxUse=10M" \
                   "20M" "SystemMaxUse=20M" \
                   "50M" "SystemMaxUse=50M" \
                   "100M" "SystemMaxUse=100M" \
                   "200M" "SystemMaxUse=200M" \
                   "$_Disable" "Storage=none" 2>${ANSWER}

                 if [[ $(cat ${ANSWER}) != "" ]]; then
                     if [[ $(cat ${ANSWER}) == "$_Disable" ]]; then
                         sed -i "s/#Storage.*\|Storage.*/Storage=none/g" ${MOUNTPOINT}/etc/systemd/journald.conf
                         sed -i "s/SystemMaxUse.*/#&/g" ${MOUNTPOINT}/etc/systemd/journald.conf
                         DIALOG " $_SecJournTitle " --infobox "\n$_Done!\n " 0 0
                         sleep 2
                     elif [[ $(cat ${ANSWER}) == "$_Edit" ]]; then
                         nano ${MOUNTPOINT}/etc/systemd/journald.conf
                     else
                         sed -i "s/#SystemMaxUse.*\|SystemMaxUse.*/SystemMaxUse=$(cat ${ANSWER})/g" ${MOUNTPOINT}/etc/systemd/journald.conf
                         sed -i "s/Storage.*/#&/g" ${MOUNTPOINT}/etc/systemd/journald.conf
                         DIALOG " $_SecJournTitle " --infobox "\n$_Done!\n " 0 0
                         sleep 2
                     fi
                 fi
                 ;;
            # core dump
            "2") DIALOG " $_SecCoreTitle " --menu "\n$_SecCoreBody\n " 0 0 2 \
                 "$_Disable" "Storage=none" \
                 "$_Edit" "/etc/systemd/coredump.conf" 2>${ANSWER}

                 if [[ $(cat ${ANSWER}) == "$_Disable" ]]; then
                     sed -i "s/#Storage.*\|Storage.*/Storage=none/g" ${MOUNTPOINT}/etc/systemd/coredump.conf
                     DIALOG " $_SecCoreTitle " --infobox "\n$_Done!\n " 0 0
                     sleep 2
                 elif [[ $(cat ${ANSWER}) == "$_Edit" ]]; then
                     nano ${MOUNTPOINT}/etc/systemd/coredump.conf
                 fi
                 ;;
            # Kernel log access
            "3") DIALOG " $_SecKernTitle " --menu "\n$_SecKernBody\n " 0 0 2 \
                 "$_Disable" "kernel.dmesg_restrict = 1" \
                 "$_Edit" "/etc/systemd/coredump.conf.d/custom.conf" 2>${ANSWER}

                  case $(cat ${ANSWER}) in
                      "$_Disable") echo "kernel.dmesg_restrict = 1" > ${MOUNTPOINT}/etc/sysctl.d/50-dmesg-restrict.conf
                                   DIALOG " $_SecKernTitle " --infobox "\n$_Done!\n " 0 0
                                   sleep 2 ;;
                      "$_Edit") [[ -e ${MOUNTPOINT}/etc/sysctl.d/50-dmesg-restrict.conf ]] && nano ${MOUNTPOINT}/etc/sysctl.d/50-dmesg-restrict.conf || \
                                  DIALOG " $_SeeConfErrTitle " --msgbox "\n$_SeeConfErrBody1\n " 0 0 ;;
                  esac
                 ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

enable_console_logging() {
  echo "ForwardToConsole=yes
TTYPath=/dev/tty12" >> /mnt/etc/systemd/jounald.conf
  sed -i '/MaxLevelConsole/ s/#//' /mnt/etc/systemd/journald.conf
}

enable_autologin() {
  dm=$(file /mnt/etc/systemd/system/display-manager.service 2>/dev/null | awk -F'/' '{print $NF}' | cut -d. -f1)
  [[ -z $dm ]] && dm=xlogin
  if DIALOG " Autologin setup " --yesno "\nThis option enables autologin using $dm.\n\nProceed? \n " 0 0; then
      #detect displaymanager
      if [[ $(echo /mnt/home/* | xargs -n1 | wc -l) == 1 ]]; then
        autologin_user=$(echo /mnt/home/* | cut -d/ -f4)
      else
        autologin_user=$(echo /mnt/home/* | cut -d/ -f4 | fzf --reverse --prompt="user> " --header="Choose the user to automatically log in")
      fi
      #enable autologin
      case "$(echo $dm)" in 
        gdm)      sed -i "s/^AutomaticLogin=*/AutomaticLogin=$autologin_user/g" /mnt/etc/gdm/custom.conf
                  sed -i 's/^AutomaticLoginEnable=*/AutomaticLoginEnable=true/g' /mnt/etc/gdm/custom.conf
                  sed -i 's/^TimedLoginEnable=*/TimedLoginEnable=true/g' /mnt/etc/gdm/custom.conf
                  sed -i 's/^TimedLogin=*/TimedLoginEnable=$autologin_user/g' /mnt/etc/gdm/custom.conf
                  sed -i 's/^TimedLoginDelay=*/TimedLoginDelay=0/g' /mnt/etc/gdm/custom.conf    
            ;;
        lightdm)  sed -i "s/^#autologin-user=/autologin-user=$autologin_user/" /mnt/etc/lightdm/lightdm.conf
                  sed -i 's/^#autologin-user-timeout=0/autologin-user-timeout=0/' /mnt/etc/lightdm/lightdm.conf
                  arch_chroot "groupadd -r autologin"
                  arch_chroot "gpasswd -a $autologin_user autologin"
            ;;
        sddm)     xsession=$(echo /mnt/usr/share/xsessions/* | xargs -n1 | head -n1 | rev | cut -d'/' -f 1 | rev)
                  [[ -e /mnt/etc/sddm.conf ]] || arch_chroot "sddm --example-config > /etc/sddm.conf"
                  sed -i "s/^User=/User=$autologin_user/g" /mnt/etc/sddm.conf
                  sed -i "s~^Session=~Session=$xsession~g" /mnt/etc/sddm.conf
            ;;
        lxdm)     sed -i "s/^# autologin=dgod/autologin=$autologin_user/g" /mnt/etc/lxdm/lxdm.conf
            ;;
        *)        pacstrap ${MOUNTPOINT} "xlogin"
                  arch_chroot "systemctl enable xlogin@${autologin_user}"
            ;;
      esac
fi
}

set_schedulers() {
  [[ -e /mnt/etc/udev/rules.d/60-ioscheduler.rules ]] ||  \
  echo '# set scheduler for non-rotating disks
# noop and deadline are recommended for non-rotating disks
# for rotational disks, cfq gives better performance and bfq-sq more responsive desktop environment
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="deadline"
# set scheduler for rotating disks
ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq-sq"' > /mnt/etc/udev/rules.d/60-ioscheduler.rules
  nano /mnt/etc/udev/rules.d/60-ioscheduler.rules
}

set_swappiness() {
  [[ -e /mnt/etc/sysctl.d/99-sysctl.conf ]] ||  \
  echo 'vm.swappiness = 10
vm.vfs_cache_pressure = 50
#vm.dirty_ratio = 3' > /mnt/etc/sysctl.d/99-sysctl.conf
  nano /mnt/etc/sysctl.d/99-sysctl.conf
}

