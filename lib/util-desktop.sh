#!/bin/bash
#
# Architect Installation Framework (2016-2017)
#
# Written by Carl Duff and @mandog for Archlinux
# Heavily modified and re-written by @Chrysostomus to install Manjaro instead
# Contributors: @papajoker, @oberon and the Manjaro-Community.
#
# This program is free software, provided under the GNU General Public License
# as published by the Free Software Foundation. So feel free to copy, distribute,
# or modify it as you wish.

setup_graphics_card() {
    GRAPHIC_CARD=$(lspci | grep -i "vga" | sed 's/.*://' | sed 's/(.*//' | sed 's/^[ \t]*//')

    # All non-NVIDIA cards / virtualisation
    if [[ $(echo $GRAPHIC_CARD | grep -i 'intel\|lenovo') != "" ]]; then
        install_intel
    elif [[ $(echo $GRAPHIC_CARD | grep -i 'ati') != "" ]]; then
        install_ati
    elif [[ $(echo $GRAPHIC_CARD | grep -i 'nvidia') != "" ]]; then
        install_nvidia
    fi
}

install_intel() {
    local cmd="pacman -S --noconfirm intel-media-driver libva-intel-driver vulkan-intel"
    arch_chroot "${cmd}" 2>$ERR
    check_for_error "${cmd}" "$?"
}


install_ati() {
    local cmd="pacman -S --noconfirm libva-mesa-driver mesa-vdpau vulkan-radeon"
    arch_chroot "${cmd}" 2>$ERR
    check_for_error "${cmd}" "$?"
}

install_nvidia() {
    local cmd="pacman -S --noconfirm libva-mesa-driver mesa-vdpau"
    arch_chroot "${cmd}" 2>$ERR
    check_for_error "${cmd}" "$?"
}

# Set keymap for X11
set_xkbmap() {
    XKBMAP_LIST=""
    keymaps_xkb=("af al am at az ba bd be bg br bt bw by ca cd ch cm cn cz de dk ee es et eu fi fo fr\
      gb ge gh gn gr hr hu ie il in iq ir is it jp ke kg kh kr kz la lk lt lv ma md me mk ml mm mn mt mv\
      ng nl no np pc ph pk pl pt ro rs ru se si sk sn sy tg th tj tm tr tw tz ua us uz vn za")

    for i in ${keymaps_xkb}; do
        XKBMAP_LIST="${XKBMAP_LIST} ${i} -"
    done

    DIALOG " $_PrepKBLayout " --menu "\n$_XkbmapBody\n " 0 0 16 ${XKBMAP_LIST} 2>${ANSWER} || return 0
    XKBMAP=$(cat ${ANSWER} |sed 's/_.*//')
    echo -e "Section "\"InputClass"\"\nIdentifier "\"system-keyboard"\"\nMatchIsKeyboard "\"on"\"\nOption "\"XkbLayout"\" "\"${XKBMAP}"\"\nEndSection" \
      > ${MOUNTPOINT}/etc/X11/xorg.conf.d/00-keyboard.conf 2>$ERR
    check_for_error "$FUNCNAME ${XKBMAP}" "$?"
}

install_desktop() {
    if [[ -e /mnt/.base_installed ]]; then
        DIALOG " $_InstBseTitle " --yesno "\n$_WarnInstBase\n " 0 0 && rm /mnt/.base_installed || return 0
    fi
    # Prep variables
    touch /tmp/.git_profiles
    #setup_profiles
    PROFILES="$DATADIR/profiles"
    pkgs_src=$PROFILES/shared/Packages-Root
    pkgs_target=/mnt/.base
    BTRF_CHECK=$(echo "btrfs-progs" "" off)
    F2FS_CHECK=$(echo "f2fs-tools" "" off)
    
    # Create the base list of packages
    echo "" > /mnt/.base

    declare -i loopmenu=1
    while ((loopmenu)); do
        # Choose kernel and possibly base-devel
        DIALOG " $_InstBseTitle " --checklist "\n$_InstStandBseBody$_UseSpaceBar\n " 0 0 15 \
          "base-devel" "-" on \
          "linux" "-" on \
          "linux-lts" "-" off  2>${PACKAGES} || { loopmenu=0; return 0; }
        if [[ ! $(grep "linux" ${PACKAGES}) ]]; then
            # Check if a kernel is already installed
            ls ${MOUNTPOINT}/boot/*.img >/dev/null 2>&1
            if [[ $? == 0 ]]; then
                DIALOG " Check Kernel " --msgbox "\nlinux-$(ls ${MOUNTPOINT}/boot/*.img | cut -d'-' -f2 | grep -v ucode.img | sort -u) detected \n " 0 0
                check_for_error "linux-$(ls ${MOUNTPOINT}/boot/*.img | cut -d'-' -f2) already installed"
                loopmenu=0
            else
                DIALOG " $_ErrTitle " --msgbox "\n$_ErrNoKernel\n " 0 0
            fi
        else
            cat ${PACKAGES} | sed 's/+ \|\"//g' | tr ' ' '\n' | tr '+' '\n' >> /mnt/.base
            echo " " >> /mnt/.base
            check_for_error "selected: $(cat ${PACKAGES})"
            loopmenu=0
        fi
    done

    setup_modules

    if [[ $(cat /tmp/.modules) != "" ]]; then
        check_for_error "modules: $(cat /tmp/.modules)"
        for kernel in $(cat /mnt/.base | grep -vE '(yay|base-devel)'); do
            cat /tmp/.modules | sed "s/KERNEL/\n$kernel/g" >> /mnt/.base
        done
        echo " " >> /mnt/.base
    fi

    choose_desktop_profile

    filter_packages
    # remove grub
    sed -i '/grub/d' /mnt/.base
    #echo "nilfs-utils" >> /mnt/.base
    check_for_error "packages to install: $(cat /mnt/.base | sort | tr '\n' ' ')"
    clear
    set -o pipefail
    if ! $EDIT_PKGS; then
        vi /mnt/.base
    fi
    if $hostcache; then
        pacstrap ${MOUNTPOINT} $(cat /mnt/.base) 2>$ERR |& tee /tmp/pacstrap.log
    else
        pacstrap -c ${MOUNTPOINT} $(cat /mnt/.base) 2>$ERR |& tee /tmp/pacstrap.log
    fi
    
    local err=$?
    set +o pipefail
    check_for_error "install basepkgs" $err || {
        DIALOG " $_InstBseTitle " --msgbox "\n$_InstFail\n " 0 0; HIGHLIGHT_SUB=2;
        if [[ $err == 255 ]]; then
            cat /tmp/pacstrap.log
            read -n1 -s # or ? exit $err
        fi
        return 1;
    }

    # copy keymap and consolefont settings to target
        echo -e "KEYMAP=$(ini linux.keymap)\nFONT=$(ini linux.font)" > ${MOUNTPOINT}/etc/vconsole.conf
        check_for_error "configure vconsole"

    # mkinitcpio handling for specific filesystems
    case $(findmnt -ln -o FSTYPE ${MOUNTPOINT}) in
        btrfs)  
            BTRFS_ROOT=1
            sed -e '/^HOOKS=/s/\ fsck//g' -e '/^MODULES=/s/"$/ btrfs"/g' -i ${MOUNTPOINT}/etc/mkinitcpio.conf
            check_for_error "root on btrfs volume. Amend mkinitcpio."
            # Adjust tlp settings to avoid filesystem corruption
            if [[ -e /mnt/etc/default/tlp ]]; then
                sed -i 's/SATA_LINKPWR_ON_BAT.*/SATA_LINKPWR_ON_BAT=max_performance/' /mnt/etc/default/tlp
            fi
            ;;
        nilfs2)
            sed -e '/^HOOKS=/s/\ fsck//g' -i ${MOUNTPOINT}/etc/mkinitcpio.conf
            check_for_error "root on nilfs2 volume. Amend mkinitcpio."
            ;;
        zfs)
            ZFS_ROOT=1
            # the order is important here so strip out what we want changed and put it back in the correct order
            sed -e '/^HOOKS=/s/\ filesystems//g' -e '/^HOOKS=/s/\ keyboard/\ keyboard\ zfs\ filesystems/g' -e '/^HOOKS=/s/\ fsck//g' -e '/^FILES=/c\FILES=("/usr/lib/libgcc_s.so.1")' -i ${MOUNTPOINT}/etc/mkinitcpio.conf
            check_for_error "root on zfs volume. Amend mkinitcpio."
            ;;
        *)
            if $FSCK_HOOK; then
                # Remove fsck unless chosen otherwise
                sed -e '/^HOOKS=/s/\ fsck//g' -i ${MOUNTPOINT}/etc/mkinitcpio.conf
                check_for_error "no fsck specified. Removing fsck hook from mkinitcpio.conf."
            fi

            ;;
    esac
    
    
    # check to see if raid is needed for boot
    # if mount point is on raid then it is needed
    if [[ $(lsblk -lno TYPE,MOUNTPOINT | grep -E "raid.*${MOUNTPOINT}" | wc -l)> 0 ]]; then
        raid_needed=true
        
    # put all the lines of lsblk, before the mountpoint, into an array
    # iterate through the array backwards util a partition is reached
    # if raid was involved anywere between the mountpoint and partition, 
    # then inital ramdisk configuration for raid is need
    else
    
        old_ifs="$IFS"
        IFS=$'\n'
        lsblk_lines=($(lsblk -lno TYPE,NAME,MOUNTPOINT | sed  "/\/${MOUNTPOINT:1}$/q"))
        IFS="$old_ifs"
        
        for (( i=${#lsblk_lines[@]}-1 ; i>=0 ; i-- )) ; do
            if [[ $(echo ${lsblk_lines[i]} | grep "^lvm" | wc -l) > 0 ]]; then
                    sed -i '/lvm2/b; s/\<block\>/& lvm2/' ${MOUNTPOINT}/etc/mkinitcpio.conf
                    continue
            fi
            if [[ $(echo ${lsblk_lines[i]} | grep "^crypt" | wc -l) > 0 ]]; then
                    sed -i '/encrypt/b; s/\<block\>/& encrypt/' ${MOUNTPOINT}/etc/mkinitcpio.conf
                    sed -i '/keymap/b; s/\<autodetect\>/& keymap/' ${MOUNTPOINT}/etc/mkinitcpio.conf
                    sed -i '/keyboard/b; s/\<autodetect\>/& keyboard/' ${MOUNTPOINT}/etc/mkinitcpio.conf
                    continue
            fi
            if [[ $(echo ${lsblk_lines[i]} | grep "^raid" | wc -l) > 0 ]]; then
                    raid_needed=true
                    raid_device_name=$(echo ${lsblk_lines[i]} | cut -f2 -d' ')
                    continue
            fi
            if [[ $(echo ${lsblk_lines[i]} | grep "^part" | wc -l) > 0 ]]; then
                    break
            fi
        done
        
    fi
    
    
    # add mkinitcpio raid binary and hook, if root partition is on raid
    if [ "$raid_needed" = true ]; then

        # auto assemble raid
        mdadm --detail --scan >> ${MOUNTPOINT}/etc/mdadm.conf
        
        # add raid initramfs hook 
        sed -i 's/\<block\>/& mdadm_udev/' ${MOUNTPOINT}/etc/mkinitcpio.conf
        binaries_line_number=$(grep -n "^BINARIES=(" ${MOUNTPOINT}/etc/mkinitcpio.conf | cut -f1 -d':')
        sed -i "${binaries_line_number}s/^\(.\{10\}\)/\1mdmon/" ${MOUNTPOINT}/etc/mkinitcpio.conf

        # get newest kernel and initramfs
        newest_kernel=$(ls ${MOUNTPOINT}/lib/modules | grep '^[0-9]' | sort | tail -n 1)
        newest_initramfs=$(ls ${MOUNTPOINT}/boot | grep "initramfs" | grep -v "fallback"| sort | tail -n 1)
        
        # initramfs needs to be recomiled with raid support
        arch-chroot ${MOUNTPOINT} mkinitcpio -c /etc/mkinitcpio.conf -g /boot/${newest_initramfs} -k ${newest_kernel}
    
    fi    
    
    recheck_luks
    
    # add luks and lvm hooks as needed
    ([[ $LVM -eq 1 ]] && [[ $LUKS -eq 0 ]]) && { sed -i 's/block filesystems/block lvm2 filesystems/g' ${MOUNTPOINT}/etc/mkinitcpio.conf 2>$ERR; check_for_error "add lvm2 hook" $?; }
    ([[ $LVM -eq 0 ]] && [[ $LUKS -eq 1 ]]) && { sed -i 's/block filesystems keyboard/block consolefont keymap keyboard encrypt filesystems/g' ${MOUNTPOINT}/etc/mkinitcpio.conf 2>$ERR; check_for_error "add luks hook" $?; }
    [[ $((LVM + LUKS)) -eq 2 ]] && { sed -i 's/block filesystems keyboard/block consolefont keymap keyboard encrypt lvm2 filesystems/g' ${MOUNTPOINT}/etc/mkinitcpio.conf 2>$ERR; check_for_error "add lvm/luks hooks" $?; }

    [[ $((LVM + LUKS + BTRFS_ROOT + ZFS_ROOT)) -gt 0 ]] && { arch_chroot "mkinitcpio -P" 2>$ERR; check_for_error "re-run mkinitcpio" $?; }

    # Generate fstab with UUID
    genfstab -U -p ${MOUNTPOINT} > ${MOUNTPOINT}/etc/fstab
    # Edit fstab in case of btrfs subvolumes
    sed -i "s/subvolid=.*,subvol=\/.*,//g" /mnt/etc/fstab

    # If specified, copy over the pacman.conf file to the installation
    if [[ $COPY_PACCONF -eq 1 ]]; then
        cp -f /etc/pacman.conf ${MOUNTPOINT}/etc/pacman.conf
        check_for_error "copy pacman.conf"
    fi

    # if branch was chosen, use that also in installed system. If not, use the system setting
    # [[ -z $(ini branch) ]] && ini branch $(ini system.branch)
    # sed -i "s/Branch =.*/Branch = $(ini branch)/;s/# //" ${MOUNTPOINT}/etc/pacman-mirrors.conf

    touch /mnt/.base_installed
    check_for_error "base installed succesfully."


    if [[ -n ${user_dotfiles} ]]; then
        echo "Installing dotfiles to overlay"
        install_dotfiles
    fi

    ## Setup desktop
    if [[ $(cat /tmp/.desktop) != "" ]]; then
    # copy the profile overlay to the new root
        echo "Copying overlay files to the new root"

        if [[ ${shared_overlay} == "true" ]]; then
           echo "Using shared overlay"
           cp -ar ${PROFILES}/shared/desktop-overlay/*  ${MOUNTPOINT} 2>$ERR
        fi

        cp -r "$overlay"* ${MOUNTPOINT} 2>$ERR
        check_for_error "copy overlay" "$?"

        # Copy settings to root account
        cp -ar $MOUNTPOINT/etc/skel/. $MOUNTPOINT/root/ 2>$ERR
        check_for_error "copy root config" "$?"

        # copy settings to already created users
        if [[ -e "$(echo /mnt/home/*)" ]]; then
            for home in $(echo $MOUNTPOINT/home/*); do
                cp -ar $MOUNTPOINT/etc/skel/. $home/
                user=$(echo $home | cut -d/ -f4)
                arch_chroot "chown -R ${user}:${user} /home/${user}"
            done
        fi

        if [[ ${aur_support} == "true" ]]; then
            install_aur_packages
        fi
        
        # Enable services in the chosen profile
        enable_services

        # If the overlay contains dconf user profile (Gnome desktop),
        # update the system databases
        if [[ -e /mnt/etc/dconf/profile/user ]]; then
            echo "updating system databases"
            arch_chroot "dconf update"
            check_for_error "dconf update" "$?"
        fi
        
        echo "enabling system clock synchronization"
        arch_chroot "timedatectl set-ntp true"
        check_for_error "timedatectl set-ntp true" "$?"
    
        setup_graphics_card

        run_post_install_hooks
        
        touch /mnt/.desktop_installed
        # Stop for a moment so user can see if there were errors
        echo ""
        echo ""
        echo ""
        echo "press Enter to continue"
        read
    # else 
    #     install_network_drivers
    fi  
}

choose_desktop_profile() {
    # Clear packages after installing base
    echo "" > /tmp/.desktop

    # DE/WM Menu
    DIALOG " $_InstDETitle " --radiolist "\n$_InstManDEBody\n\n$_UseSpaceBar\n " 0 0 15 \
      $(echo $PROFILES/profiles/* | xargs -n1 | cut -f4 -d'/' | grep -vE "netinstall|architect" | awk '$0=$0" - off"')  2> /tmp/.desktop

    # If something has been selected, install
    if [[ $(cat /tmp/.desktop) != "" ]]; then
        check_for_error "selected: [$(cat /tmp/.desktop)]"
        clear
        # Source the iso-profile
        profile=$(echo $PROFILES/*/$(cat /tmp/.desktop)/profile.conf)
        . $profile        
        overlay=$(echo $PROFILES/*/$(cat /tmp/.desktop)/desktop-overlay/)
        echo $displaymanager > /tmp/.display-manager

        profile_pkg_root=$(echo $PROFILES/*/$(cat /tmp/.desktop)/Packages-Root)
        if [[ -e ${profile_pkg_root} ]]; then
            echo "Switching to Packages-Root from profile"
            pkgs_src=${profile_pkg_root}
        fi

        cat $(echo $PROFILES/*/$(cat /tmp/.desktop)/Packages-Desktop) > /mnt/.desktop
        echo "" >> /mnt/.desktop
        DIALOG " $_ExtraPkgTitle " --yesno "\n$_ExtraPkgBody \n " 0 0 && \
        echo "$(pacman -Ssq) $(pacman -Sg)" | fzf -m -e --header="$_AddPkgs" --prompt="$_AddPkgsPrmpt > " --reverse >> /mnt/.desktop && EDIT_PKGS=false
        
    fi
}

install_dotfiles() {
    if [[ -n ${profile} ]]; then
        profile=$(echo $PROFILES/*/$(cat /tmp/.desktop)/profile.conf)
        . $profile
        overlay=$(echo $PROFILES/*/$(cat /tmp/.desktop)/desktop-overlay/)
    fi

    if [[ ! -e ${overlay}etc/skel/.config ]]; then
        mkdir -p ${overlay}etc/skel/.config
    fi

    (
        cd ${overlay}etc/skel || return
        git clone ${user_dotfiles} .dotfiles

        for cmd in "${install_dotfiles_commands[@]}"; do
            $cmd
        done
    )
}

install_aur_packages() {
    # Install aur packages in the chosen profile
    echo "installing yay and aur packages"
    cat >  ${MOUNTPOINT}/usr/bin/aur-installer.sh <<EOF
#!/bin/bash
useradd -m -s /bin/bash instuser
sed -i '\$a instuser ALL=(ALL) NOPASSWD:/usr/bin/pacman,/usr/bin/yay' /etc/sudoers
su instuser -c 'git clone https://aur.archlinux.org/yay-bin.git /tmp/yay-bin'
cd /tmp/yay-bin
su instuser -c 'makepkg -si --noconfirm'
EOF

    echo "${aur_packages[@]}" | xargs -n1 > /tmp/.aur_packages
    for aur_package in $(cat /tmp/.aur_packages); do
        echo "su instuser -c 'yay -S ${aur_package} --noconfirm'" >> ${MOUNTPOINT}/usr/bin/aur-installer.sh
    done

    echo -e "sed -i '/^instuser.*$/d' /etc/sudoers
userdel -r instuser" >> ${MOUNTPOINT}/usr/bin/aur-installer.sh
    
    [[ -f ${MOUNTPOINT}/usr/bin/aur-installer.sh ]] && chmod a+x ${MOUNTPOINT}/usr/bin/aur-installer.sh

    arch_chroot "aur-installer.sh" 2>$ERR

    
    [[ -f ${MOUNTPOINT}/usr/bin/aur-installer.sh ]] && rm ${MOUNTPOINT}/usr/bin/aur-installer.sh
}

run_post_install_hooks() {
    for cmd in "${post_install_hooks[@]}"; do
        arch_chroot "${cmd}" 2>$ERR
        check_for_error "${cmd}" "$?"
    done
}

set_lightdm_greeter() {
    local greeters=$(ls /mnt/usr/share/xgreeters/*greeter.desktop) name
    for g in ${greeters[@]}; do
        name=${g##*/}
        name=${name%%.*}
        case ${name} in
            lightdm-gtk-greeter)
                break
                ;;
            lightdm-*-greeter)
                sed -i -e "s/^.*greeter-session=.*/greeter-session=${name}/" /mnt/etc/lightdm/lightdm.conf
                ;;
        esac
    done
}

set_sddm_ck() {
    local halt='/usr/bin/shutdown -h -P now' \
      reboot='/usr/bin/shutdown -r now'
    sed -e "s|^.*HaltCommand=.*|HaltCommand=${halt}|" \
      -e "s|^.*RebootCommand=.*|RebootCommand=${reboot}|" \
      -e "s|^.*MinimumVT=.*|MinimumVT=7|" \
      -i "/mnt/etc/sddm.conf"
    arch_chroot "gpasswd -a sddm video" 2>$ERR
    check_for_error "$FUNCNAME" $?
}

setup_modules() {

    echo "KERNEL-headers"  >> /tmp/.modules 

    if systemd-detect-virt | grep -q "oracle"; then
         # For virtualbox
         echo "virtualbox-guest-dkms" >> /tmp/.modules
    fi 

    if systemd-detect-virt | grep -q "kvm"; then
         # For libvirt/kvm
         echo "spice-vdagent" >> /tmp/.modules
    fi 

    if dmidecode -t system | grep -q -i "ThinkPad"; then
        # For thinkpads
        echo "tp_smapi" >> /tmp/.modules
    fi

    if lspci | grep -i -q broadcom; then
        # For broadcom  wifi card
        echo "broadcom-wl-dkms" >> /tmp/.modules
    fi
    
}
