# !/bin/bash
#
# Architect Installation Framework (2016-2017)
#
# Written by Carl Duff and @mandog for Archlinux
# Heavily modified and re-written by @Chrysostomus to install Manjaro instead
# Contributors: @papajoker, @oberon and the Manjaro-Community.
#
# This program is free software, provided under the GNU General Public License
# as published by the Free Software Foundation. So feel free to copy, distribute,
# or modify it as you wish.

import ${LIBDIR}/util-desktop.sh 

main_menu() {
    declare -i loopmenu=1
    while ((loopmenu)); do
        if [[ $HIGHLIGHT != 6 ]]; then
           HIGHLIGHT=$(( HIGHLIGHT + 1 ))
        fi

        DIALOG " $_MMTitle " --default-item ${HIGHLIGHT} \
          --menu "\n$_MMNewBody\n " 20 60 6 \
          "1" "$_PrepMenuTitle|>" \
          "2" "$_InstDsMenuTitle|>" \
          "3" "$_SysRescTitle|>" \
          "4" "$_Done" 2>${ANSWER}
        HIGHLIGHT=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") prep_menu
                ;;
            "2") check_mount && install_desktop_system_menu
                ;;
            # "3") check_mount && install_core_menu
            #     ;;
            # "4") check_mount && install_custom_menu
            #     ;;
            "3") system_rescue_menu
                ;;
             *) loopmenu=0
                exit_done
                ;;
        esac
    done
}


install_desktop_system_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 7
        DIALOG " $_InstDsMenuTitle " --default-item ${HIGHLIGHT_SUB} --menu "\n$_MMBody\n$_InstDsMenuBody\n " 22 60 7 \
          "1" "$_InstDEStable|>" \
          "2" "$_InstBootldr|>" \
          "3" "$_ConfBseMenuTitle|>" \
          "4" "$_TweaksMenuTitle|>" \
          "5" "$_SeeConfOptTitle|>" \
          "6" "$_ChrootTitle" \
          "7" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") check_mount && install_desktop
                 ;;
            "2") check_base && install_bootloader
                 ;;
            "3") check_base && config_base_menu
                 ;;
            "4") check_base && tweaks_menu
                ;;
            "5") check_base && {
                    type edit_configs &>/dev/null || import ${LIBDIR}/util-config.sh
                    edit_configs
                    }
                ;;
            "6") check_base && chroot_interactive
                ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}


system_rescue_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 8
        DIALOG " $_SysRescTitle " --default-item ${HIGHLIGHT_SUB} --menu "\n$_SysRescBody\n " 24 60 10 \
          "1" "$_InstBootldr|>" \
          "2" "$_ConfBseMenuTitle" \
          "3" "$_InstMulCust" \
          "4" "$_RmPkgs" \
          "5" "$_SeeConfOptTitle|>" \
          "6" "$_ChrootTitle" \
          "7" "$_DataRecMenu|>" \
          "8" "$_LogMenu|>" \
          "9" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") check_mount && check_base && install_bootloader
                 ;;
            "2") check_mount && check_base && config_base_menu
                 ;;
            "3") check_mount && install_cust_pkgs
                 ;;
            "4") check_mount && check_base && rm_pgs
                 ;;
            "5") check_mount && check_base && {
                    type edit_configs &>/dev/null || import ${LIBDIR}/util-config.sh
                    edit_configs
                    }
                ;;
            "6") check_mount && check_base && chroot_interactive
                ;;
            "7") recovery_menu
                ;;
            "8") check_mount && check_base && logs_menu
                ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

# Preparation
prep_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 13
        DIALOG " $_PrepMenuTitle " --default-item ${HIGHLIGHT_SUB} --menu "\n$_PrepMenuBody\n " 0 0 0 \
          "1" "$_PrepExpress" \
          "2" "$_VCKeymapTitle" \
          "3" "$_DevShowOpt" \
          "4" "$_PrepPartDisk|>" \
          "5" "$_PrepLUKS|>" \
	      "6" "$_PrepLVM $_PrepLVM2|>" \
	      "7" "$_PrepRAID|>" \
          "8" "$_PrepMntPart" \
          "9" "$_PrepMirror|>" \
          "10" "$_PrepPacKey" \
          "11" "$_HostCache" \
          "12" "Enable fsck hook" \
          "13" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") express_prep_menu
                 ;;
            "2") select_keymap
                 set_keymap
                 ;;
            "3") show_devices
                 ;;
            "4") umount_partitions
                 select_device && create_partitions
                 ;;
            "5") luks_menu
                 ;;
            "6") lvm_menu
                 ;;
            "7") raid_level_menu
                 ;;
            "8") mount_partitions
                 ;;
            "9") configure_mirrorlist
                 ;;
            "10") clear
                 (
                    ctrlc(){
                      return 0
                    }
                    trap ctrlc SIGINT
                    trap ctrlc SIGTERM
                    pacman-key --init;pacman-key --populate archlinux;pacman-key --refresh-keys;
                    check_for_error 'refresh pacman-keys'
                  )
                 ;;
            "11") set_cache
                 ;;
            "12") set_fsck_hook		
                 ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

express_prep_menu() {
    declare -i loopmenu=1
    while ((loopmenu)); do
        ssubmenu 2
        DIALOG " $_PrepExpress " --default-item ${HIGHLIGHT_SSUB} --menu "\n$_PrepExpressMenuBody\n" 0 0 2 \
          "1" "UEFI, lvm on luks with keyfile" \
          "2" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SSUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") express_uefi_lvm_luks_keyfile
                ;;
            *) loopmenu=0
               return 0
                ;;
        esac
    done
}

# Fsck hook

set_fsck_hook() {
    DIALOG " Set fsck hook " --yesno "\nDo you want to use fsck hook?\n " 0 0
    if [[ $? -eq 0 ]]; then
      FSCK_HOOK=false
    else
      FSCK_HOOK=true
    fi
}

# Base Configuration
config_base_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        ssubmenu 8
        DIALOG " $_ConfBseMenuTitle " --default-item ${HIGHLIGHT_SSUB} --menu "\n$_ConfBseBody\n " 0 0 8 \
          "1" "$_ConfBseFstab" \
          "2" "$_ConfBseHost" \
          "3" "$_ConfBseSysLoc" \
          "4" "$_PrepKBLayout" \
          "5" "$_ConfBseTimeHC" \
          "6" "$_ConfUsrRoot" \
          "7" "$_ConfUsrNew" \
          "8" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SSUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") generate_fstab
                ;;
            "2") set_hostname
                ;;
            "3") set_locale
                ;;
            "4") set_xkbmap
                ;;
            "5") set_timezone && set_hw_clock
                ;;
            "6") set_root_password
                ;;
            "7") create_new_user
                ;;
            *) loopmenu=0
                return 0
                ;;
        esac
    done
}

tweaks_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 7
        DIALOG " $_TweaksMenuTitle " --default-item ${HIGHLIGHT_SUB} --menu "\n$_TweaksBody " 0 0 5 \
          "1" "$_AutologEnable" \
          "2" "$_PerfMenu|>" \
          "3" "$_SecMenuTitle|>" \
          "4" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") enable_autologin
                 ;;
            #"2") enable_hibernation   removed since hibernator is not is arch main repo
            #     ;;
            "2") performance_menu
                 ;;
            "3") security_menu
                ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

performance_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 3
        DIALOG " $_PerfMenu " --default-item ${HIGHLIGHT_SUB} --menu "\n$_PerfBody\n " 0 0 4 \
          "1" "$_SetSchd" \
          "2" "$_SetSwap" \
          "3" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") set_schedulers
                 ;;
            "2") set_swappiness
                 ;;
            # "3") preloader  # Removed since preload is not in the main arch repos 
            #      ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

recovery_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 3
        DIALOG " $_DataRecMenu " --default-item ${HIGHLIGHT_SUB} --menu "\n$_DataRecBody\n " 0 0 3 \
          "1" "Clonezilla" \
          "2" "Photorec" \
          "3" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") if which clonezilla &>/dev/null; then
                    clonezilla
                 else
                    pacman -S clonezilla && clonezilla
                 fi
                 ;;
            "2") if which photorec &>/dev/null; then
                    photorec
                 else
                    pacman -S photorec && photorec
                 fi
                 ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}

logs_menu() {
    local PARENT="$FUNCNAME"
    declare -i loopmenu=1
    while ((loopmenu)); do
        submenu 3
        DIALOG " $_LogMenu " --default-item ${HIGHLIGHT_SUB} --menu "\n$_LogBody\n " 0 0 5 \
          "1" "Dmesg" \
          "2" "Pacman log" \
          "3" "Xorg log" \
          "4" "Journalctl" \
          "5" "$_Back" 2>${ANSWER}
        HIGHLIGHT_SUB=$(cat ${ANSWER})

        case $(cat ${ANSWER}) in
            "1") arch_chroot "dmesg" | fzf --reverse --header="Exit by pressing esc" --prompt="Type to filter log entries > "
                 ;;
            "2") fzf --reverse --header="Exit by pressing esc" --prompt="Type to filter log entries > " < /mnt/var/log/pacman.log
                 ;;
            "3") fzf --reverse --header="Exit by pressing esc" --prompt="Type to filter log entries > " < /mnt/var/log/Xorg.0.log
                 ;;
            "4") arch_chroot "journalctl" | fzf --reverse --header="Exit by pressing esc" --prompt="Type to filter log entries > "
                 ;;
            *) loopmenu=0
                return 0
                 ;;
        esac
    done
}
